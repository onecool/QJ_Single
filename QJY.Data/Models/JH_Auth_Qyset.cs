﻿using System;
using System.Collections.Generic;

namespace QJY.Data
{
    public partial class JH_Auth_Qyset
    {
        public int ComId { get; set; }
        public string CRUser { get; set; }
        public DateTime? CRDate { get; set; }
        public string Class { get; set; }
        public string Option { get; set; }
        public string ComID { get; set; }
    }
}
