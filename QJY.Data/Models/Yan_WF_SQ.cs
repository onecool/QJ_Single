﻿using System;
using System.Collections.Generic;

namespace QJY.Data
{
    public partial class Yan_WF_SQ
    {
        public int? ComId { get; set; }
        public int ID { get; set; }

        public int Seq { get; set; }

        public string Cate { get; set; }
        public string IsDetailUser { get; set; }
        public string DateNo { get; set; }
        public string CRUser { get; set; }
        public DateTime? CRDate { get; set; }
        public string Remark { get; set; }
    }
}
